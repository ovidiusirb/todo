from ToDo import ToDo
import simplejson
import jsonpickle

class JsonFile(object):
    def __init__(self):
        self.fileName = "toDo.json"

    def getToDos(self):
        #dataJson = []
        with open(self.fileName) as dataFile:
           #if len(dataFile.readline()) > 0:
            dataJson = simplejson.load(dataFile)
        toDos=[]
        for json in dataJson:
            message= json["message"]
            id= json["id"]
            if str(json["done"])=="True":
                done= True
            else:
                done=False
            created= json["created"]
            toDos.append(ToDo(message,id,done,created))
        return toDos

    def rewriteFile(self, toDos):
        json='['
        json+=','.join({jsonpickle.encode(todo, unpicklable=False) for todo in toDos})
        json+=']'
        f = open(self.fileName, 'w')
        f.write(json)
        f.close()

