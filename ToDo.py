class ToDo(object):
    def __init__(self,message, id_=None, done=False, created=None):
        self.message =  message
        self.id = id_
        self.done = done
        self.created = created

    def __str__(self):
        return str(self.id) + " " + self.message + " " + self.created + " " + str(self.done)
